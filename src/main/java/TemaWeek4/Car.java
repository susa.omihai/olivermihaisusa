package TemaWeek4;

public abstract class Car implements VehicleFunctions {

    public String carBrand;
    public String carModel;
    public int fuelTankSize;
    public String fuelType;
    public int totalGears;
    public double consumption;
    public double availableFuel;
    public int tireSize;
    public String chassisNumber;
    public double consumptionIncreasePerGear;


    public String getVehicle() {
        return carBrand + " " + carModel;
    }

    public Car(double availableFuel, int tireSize, String chassisNumber) {
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.chassisNumber = chassisNumber;
    }


    @Override
    public void start() {
        if (availableFuel <= 0) {
            System.out.println("No enough fuel to start the car!");
        } else {
            System.out.println("Vehicle " + getVehicle() + " started!");
            System.out.println("Available fuel is: " + availableFuel + " liters");
        }
    }

    @Override
    public void availableFuel() {
        System.out.println("Available fuel after drive is: " + availableFuel + " liters");
    }

    @Override
    public void stop() {
        System.out.println("Vehicle " + getVehicle() + " stopped!");
    }

    @Override
    public void drive(double distance) {
        if (availableFuel <= consumption) {
            System.out.println("To low on fuel to drive!");
        }
        else {
            System.out.println("Vehicle drove for " + distance + "km" + " with a consumption of " + consumption + " liters of fuel.");
    }
        double fuelConsumed = ((distance * consumption) / 100);
        System.out.println("Fuel consumed for this drive is: " + fuelConsumed + " liters");
        this.availableFuel = availableFuel - fuelConsumed;
    }

    @Override
    public void shiftGear(int gearNumber) {
        if (gearNumber > totalGears){
            System.out.println("Can't shift into that gear. Your car has a maximum of " + totalGears + " gears");
        }
        else{
            System.out.println("Car shifted to gear: " + gearNumber);
        }
        double consumptionIncrease = gearNumber * consumptionIncreasePerGear;
        System.out.println("Consumption was increased by " + consumptionIncrease + " liters");
        this.consumption = consumption + consumptionIncrease;
    }



    @Override
    public String toString() {
        return "Car{" +
                "carBrand='" + carBrand + '\'' +
                ", carModel='" + carModel + '\'' +
                ", gears=" + totalGears +
                ", tireSize=" + tireSize +
                ", fuelType='" + fuelType + '\'' +
                ", fuelTankSize=" + fuelTankSize +
                ", availableFuel=" + availableFuel +
                ", consumption=" + consumption +
                ", chassisNumber='" + chassisNumber + '\'' +
                '}';
    }
}
