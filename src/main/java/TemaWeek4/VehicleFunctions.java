package TemaWeek4;

public interface VehicleFunctions {

    void start();
    void stop();
    void drive(double distance);
    void shiftGear(int gearNumber);
    void availableFuel();


}
