package TemaWeek4.Trabant;

import TemaWeek4.Car;

public abstract class Trabant extends Car {

    public Trabant(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carBrand = "Trabant";
    }
}
