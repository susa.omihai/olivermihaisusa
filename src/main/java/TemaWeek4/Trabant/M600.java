package TemaWeek4.Trabant;

public class M600 extends Trabant{

    public M600(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carModel = "M600";
        this.totalGears = 4;
        this.fuelType = "Petrol";
        this.fuelTankSize = 45;
        this.consumption = 6;
    }


    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive(double distance) {
        super.drive(distance);
    }

    @Override
    public void shiftGear(int gearNumber) {
        super.shiftGear(gearNumber);
    }
}
