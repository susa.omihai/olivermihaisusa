package TemaWeek4.Trabant;

public class P50 extends Trabant{

    public P50(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carModel = "P50";
        this.totalGears = 5;
        this.fuelType = "Electric";
        this.fuelTankSize = 0;
        this.consumption = 0;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive(double distance) {
        super.drive(distance);
    }

    @Override
    public void shiftGear(int gearNumber) {
        super.shiftGear(gearNumber);
    }
}
