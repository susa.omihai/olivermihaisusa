package TemaWeek4;

import TemaWeek4.Dacia.Duster;
import TemaWeek4.Trabant.P50;

public class Main {
    public static void main(String[] args) {

        Car car1 = new Duster(55, 15, "123456");
        car1.start();
        car1.shiftGear(1);
        car1.drive(1);
        car1.shiftGear(2);
        car1.drive(2);
        car1.shiftGear(3);
        car1.drive(5);
        car1.shiftGear(4);
        car1.drive(5);
        car1.shiftGear(5);
        car1.drive(10);
        car1.shiftGear(4);
        car1.drive(5);
        car1.shiftGear(3);
        car1.drive(1);
        car1.stop();
        car1.availableFuel();

        Car car2 = new P50(40, 16, "789asd258");
        car2.start();
        car2.drive(2);
        car2.stop();
        car2.availableFuel();

    }
}
