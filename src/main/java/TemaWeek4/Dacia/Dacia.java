package TemaWeek4.Dacia;

import TemaWeek4.Car;

public abstract class Dacia extends Car {

    public Dacia(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carBrand = "Dacia";
    }
}
