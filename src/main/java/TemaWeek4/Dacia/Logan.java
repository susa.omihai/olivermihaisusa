package TemaWeek4.Dacia;

public class Logan extends Dacia{

    public Logan(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carModel = "Logan";
        this.totalGears = 5;
        this.fuelType = "Petrol";
        this.fuelTankSize = 50;
        this.consumption = 4.5;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void drive(double distance) {
        super.drive(distance);
    }

    @Override
    public void shiftGear(int gearNumber) {
        super.shiftGear(gearNumber);
    }
}
