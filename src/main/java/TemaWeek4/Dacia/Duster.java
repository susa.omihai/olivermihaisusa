package TemaWeek4.Dacia;

public class Duster extends Dacia {

    public Duster(double availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
        this.carModel = "Duster";
        this.totalGears = 6;
        this.fuelType = "Diesel";
        this.fuelTankSize = 60;
        this.consumption = 5;
        this.consumptionIncreasePerGear = 0.1;
    }


    @Override
    public String getVehicle() {
        return super.getVehicle();
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        super.stop();

    }

    @Override
    public void drive(double distance) {
        super.drive(distance);
    }

    @Override
    public void shiftGear(int gearNumber) {
        super.shiftGear(gearNumber);
    }

}
