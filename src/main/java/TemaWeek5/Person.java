package TemaWeek5;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {

    private String name;
    private int age;
    private int CNP;
    public List<Hobby> personHobbies = new ArrayList<>();


    public Person(String name, int age, int CNP) {
        this.name = name;
        this.age = age;
        this.CNP = CNP;
    }

    public void addHobby(Hobby hobby) {
        this.personHobbies.add(hobby);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCNP() {
        return CNP;
    }

    public void setCNP(int CNP) {
        this.CNP = CNP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return CNP == person.CNP && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", CNP=" + CNP +
                ", hobbies=" + personHobbies +
                '}';
    }
}
