package TemaWeek5;

import java.util.Comparator;

public class PersonComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Person person1 = (Person) o1;
        Person person2 = (Person) o2;

        int NameCompare = person1.getName().compareTo(person2.getName());
        int CNPCompare = Integer.compare(person1.getCNP(), person2.getCNP());

        return (NameCompare == 0) ? CNPCompare
                : NameCompare;

    }
}
