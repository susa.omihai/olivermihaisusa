package TemaWeek5;

import java.util.ArrayList;
import java.util.List;

public class Hobby {

    private String hobbyName;
    private int frequency;
    List<Address> addresses = new ArrayList<>();

    public void addAddress(Address address){
        this.addresses.add(address);
    }

    public Hobby(String hobbyName, int frequency) {
        this.hobbyName = hobbyName;
        this.frequency = frequency;
    }

    public String getHobbyName() {
        return hobbyName;
    }

    public void setHobbyName(String hobbyName) {
        this.hobbyName = hobbyName;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobbyName='" + hobbyName + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                '}';
    }
}
