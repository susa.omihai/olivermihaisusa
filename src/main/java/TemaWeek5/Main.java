package TemaWeek5;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        System.out.println("HI");

//Create Persons

        Person person1 = new Person("Oliver", 38, 1122334455);
        Person person2 = new Person("Mihai", 20, 1166778899);
        Person person3 = new Person("Mihai", 25, 1166778899);
        Person person4 = new Person("Georgiana", 35, 22446688);

//Create Hobbies

        Hobby running = new Hobby("running", 3);
        Hobby cycling = new Hobby("cycling", 2);


        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add(new Hobby("running", 5));
        hobbies.add(new Hobby("gaming", 3));
        hobbies.add(new Hobby("cycling" , 1));


//Add persons to TreeSet

        Set<Person> persons = new TreeSet<>(new PersonComparator());
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);

//Print name, age and CNP
        for (Person a:persons){
            System.out.println("Name " + a.getName() + " age " + a.getAge() + " CNP " + a.getCNP());
        }

//Create address
        Address address1 = new Address("Cluj", "Romania");
        Address address2 = new Address("Barnsley", "UK");
        Address address3 = new Address("New York", "USA");

//Add address to hobby
        for (Hobby hobby : hobbies){
            String hobbyName = hobby.getHobbyName();
            switch (hobbyName){
                case "running":
                    hobby.addAddress(address1);
                    hobby.addAddress(address2);
                    break;
                case "cycling":
                    hobby.addAddress(address1);
                    break;
                case "gaming":
                    hobby.addAddress(address1);
                    hobby.addAddress(address2);
                    hobby.addAddress(address3);
                    break;
            }
        }
        person1.addHobby(running);

        System.out.println(person1);


//        Map<Person, List<Hobby> > personMap = new HashMap<>();
//        personMap.put(person1, );
//
//        System.out.println(personMap);


    }
}
